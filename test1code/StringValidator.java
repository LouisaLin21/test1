/*Louisa Yuxin Lin 1933472*/ 
package test1code;
public class StringValidator{
    public static boolean isANumber(String s){
        //Initialize the boolean variable to be true
        boolean result=true;
        for(int i =0; i<s.length();i++){
            //if the negative sign is anywhere other than the first
            //place on the string, we should set the result to false and retrun.

            if(i!=0 && s.charAt(i)=='-'){
                result=false;
                return result;
            }
            //if the character at i position is not a digit, we set the 
            //result variable to false and return it. 
            else if(Character.isDigit(s.charAt(i))==false){
                result=false;
                return result;
            }
        }
        //If nothing returns false before this, 
        //return the initial value true.
        return result;
      
         
    }
}