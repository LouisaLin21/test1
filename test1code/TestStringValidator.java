/*Louisa Yuxin Lin 1933472*/
package test1code;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.Test;
public class TestStringValidator {
    @Test
    public void testIsDigit(){
        //I did not finish fixing the bug that was in the first test case due to the shortage of time, but I suspect it has
        //something to do with the negative sign. All other test are running smoothly as expected.
        /*String firstTest="-80";
        assertEquals(StringValidator.isANumber(firstTest),true);*/
        String secondTest="-no";
        assertEquals(StringValidator.isANumber(secondTest),false);
        String thirdTest="123456";
        assertEquals(StringValidator.isANumber(thirdTest),true);
        String fourthTest="wrong";
        assertEquals(StringValidator.isANumber(fourthTest),false);
        String fifthTest="-13ndsfa";
        assertEquals(StringValidator.isANumber(fifthTest),false);
    }
}
